using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : MonoBehaviour
{
    [Header("Set in Inspector")]
    [SerializeField]
    private int speed = 2;
    [SerializeField]
    private int rotSpeed = 3;
    [SerializeField]
    private string pursuedTag = "Sphere";
    [SerializeField]
    [Range(0.01f, 0.99f)]
    private float minAxelerationValue = 0.1f;
    [SerializeField]
    private float maxAxelerationDistance = 1;

    [Header("Set Dynamically")]
    [SerializeField]
    private GameObject pursuedObject;

    private Vector3 currentTargetPos;
    private Vector3 previousTargetPos;
    private float currentAxeleration;


    private void Start()
    {
        previousTargetPos = transform.position;
    }

    private void Update()
    {
        pursuedObject = FindClosestUnit();
        if (pursuedObject == null) return;

        var tragetPos = pursuedObject.transform.position;
        if(tragetPos != currentTargetPos){
            previousTargetPos = currentTargetPos;
        }
        currentTargetPos = tragetPos;

        SmoothLook(pursuedObject);

        float distBetweenNewTarget = Vector3.Distance(transform.position, currentTargetPos);
        float distBetweenPrevTarget = Vector3.Distance(transform.position, previousTargetPos);

        float currentDistance = Mathf.Min(distBetweenNewTarget, distBetweenPrevTarget);

        currentAxeleration = Mathf.Lerp(minAxelerationValue, 1f, Mathf.Min(currentDistance/maxAxelerationDistance, 1f));

        var currentSpeed = speed * currentAxeleration;

        transform.position += transform.forward * currentSpeed * Time.deltaTime;
    }

    private GameObject FindClosestUnit()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(pursuedTag);
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    private void SmoothLook(GameObject target)
    {
        var lookPos = target.transform.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotSpeed * Time.deltaTime);  
    }
}
