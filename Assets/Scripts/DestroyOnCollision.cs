using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyOnCollision : MonoBehaviour
{
    [Header("Set in Inspector")]
    [SerializeField]
    private string gameObjectTag = "Sphere";
    [SerializeField]
    private int destroyScore = 100;

    private Text scoreGT;
    private GameObject particle;
    
    private void Start()
    {   
        GameObject scoreGO = GameObject.Find("ScoreCounter");
        scoreGT = scoreGO.GetComponent<Text>();
        scoreGT.text = "0";
        particle = gameObject.transform.GetChild(0).gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        if ( other.gameObject.tag == gameObjectTag)
        {
            Destroy( other.gameObject );

            particle.SetActive(true);
            particle.GetComponent<ParticleSystem>().Play();
            int score = int.Parse( scoreGT.text );
            score += destroyScore;
            scoreGT.text = score.ToString();
        }
    }
}
