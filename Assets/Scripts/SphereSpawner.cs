using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereSpawner : MonoBehaviour
{
    [Header("Set in Inspector")]
    [SerializeField]
    private GameObject spherePrefabVar;
    [SerializeField]
    private GameObject platform;
    [SerializeField]
    [Range(0.25f, 1f)]
    private float regionSize = 0.83f;

    private Vector3 sphereScale;
    private Vector3 platformPos;
    private Vector3 platformScale;
    private Vector3 pos;
    
    private float platformBorderX;
    private float platformPosY;
    private float platformBorderZ;
    private float calculatedRegionSize;

    private void Start()
    {
        RegionCalculation();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (NeedRecalculation())
            {
                RegionCalculation();
            }
            pos = new Vector3( Random.Range(platformBorderX - platformScale.x * regionSize, platformBorderX), platformPosY, Random.Range(platformBorderZ - platformScale.z * regionSize, platformBorderZ) );
            Instantiate( spherePrefabVar, pos, Quaternion.identity );
        }
        
    }

    private void RegionCalculation()
    {
        sphereScale = spherePrefabVar.transform.localScale;
        platformPos = platform.transform.position;
        platformScale = platform.transform.localScale;
        platformBorderX = platformPos.x + (platformScale.x * regionSize)/2;
        platformPosY = platformPos.y + (platformScale.y + sphereScale.y)/2;
        platformBorderZ = platformPos.z + (platformScale.z * regionSize)/2;
        calculatedRegionSize = regionSize;
    }

    private bool NeedRecalculation()
    {
        return Mathf.Abs(calculatedRegionSize - regionSize) > 0.01f;
    }
}
